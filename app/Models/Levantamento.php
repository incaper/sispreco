<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Levantamento
 *
 * @property int $id
 * @property string $produto
 * @property int $produtor
 * @property int $municipio
 * @property int $preco
 * @property int $semana
 * @property int $mes
 * @property int $ano
 * @property string $autor
 *
 * @package App\Models
 */
class Levantamento extends Model
{
    protected $table = 'levantamentos';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'produto',
        'produtor',
        'municipio',
        'preco',
        'semana',
        'mes',
        'ano',
        'autor',
    ];
}
