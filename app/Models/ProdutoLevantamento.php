<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProdutoLevantamento
 *
 * @property int $id
 * @property string $produto
 * @property int $municipio
 * @property int $ano
 *
 * @package App\Models
 */
class ProdutoLevantamento extends Eloquent
{
    protected $connection = 'sispreco';
    protected $table = 'produtos_levantamento';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'produto',
        'municipio',
        'ano',
    ];
}
