<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UnidadeMedida
 *
 * @property int $id_unidade_medida
 * @property string $nm_unidade_medida
 * @property string $nm_sigla
 *
 * @package App\Models
 */
class UnidadeMedida extends Eloquent
{
    protected $connection = 'incaper';
    protected $table = 'unidades_medida';
    protected $primaryKey = 'id_unidade_medida';
    public $timestamps = false;

    protected $fillable = [
        'nm_unidade_medida',
        'nm_sigla',
    ];
}
