<?php

namespace App\Models;

use App\Facades\Incaper;
use Illuminate\Support\Facades\Cache;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Produto
 *
 * @property int $id_produto
 * @property string $nm_produto
 * @property string $nm_codigo_ibge
 *
 * @property UnidadeMedida $unidade_federativa
 *
 * @package App\Models
 */
class Produto extends Eloquent
{
    protected $connection = 'incaper';
    protected $table = 'produtos';
    protected $primaryKey = 'id_produto';
    public $timestamps = false;

    protected $fillable = [
        'nm_produto',
        'nm_codigo_ibge',
    ];

    public function unidade_medida()
    {
        return $this->belongsTo(UnidadeMedida::class, 'id_unidade_medida_FK');
    }

    /**
     * Retorna todos os produtos.
     *
     * @return array
     */
    public static function getAll()
    {
        $produtos = static::getFromIncaper();
        $intervalos = IntervaloPreco::all();

        foreach ($produtos as &$produto) {
            foreach ($intervalos as $intervalo) {
                if ($produto['id'] === $intervalo->produto) {
                    $produto['preco_minimo'] = $intervalo->minimo / 100.0;
                    $produto['preco_maximo'] = $intervalo->maximo / 100.0;
                }
            }

            if (!array_key_exists('preco_maximo', $produto)) {
                $produto['preco_minimo'] = 0.0;
                $produto['preco_maximo'] = 0.0;
            }
        }

        return $produtos;
    }

    /**
     * Retorna os produtos da API do Incaper.
     *
     * @return array
     */
    private static function getFromIncaper()
    {
        return Cache::remember('produtos', 10, function () {
            $response = Incaper::get('/incaper/api/produtos');

            return array_get(json_decode($response->getBody(), true), 'data', []);
        });
    }

    /**
     * Encontra o produto com o código do IBGE.
     *
     * @param string $codigoIbge
     * @return mixed
     */
    public static function getProduto($codigoIbge)
    {
        return array_first(static::getFromIncaper(), function ($produto) use ($codigoIbge) {
            return array_get($produto, 'codigo') == $codigoIbge;
        });
    }
}
