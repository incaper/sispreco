<?php

namespace App\Http\Controllers;

use App\Models\Municipio;
use App\Models\Producao;
use App\Models\Produto;
use ForceUTF8\Encoding;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use League\Csv\Reader;

class ImportarController extends Controller
{
    /**
     * @var int Offset to be used to get the header from the CSV file.
     */
    private $_csvHeaderOffset = 0;

    /**
     * @var string Delimiter character to be used for the CSV file.
     */
    private $_csvDelimiter = ',';

    /**
     * @var array Records that are pending insertion.
     */
    private $_pendingRecords = [];

    /**
     * @var int Number of records that were imported successfully.
     */
    private $_successfulRecordCount = 0;

    /**
     * @var int Number of records that failed to import.
     */
    private $_failedRecordCount = 0;

    /**
     * @var array Produtos from the Incaper API.
     */
    public $produtos;

    /**
     * @var array Municípios from the Incaper API.
     */
    public $municipios;

    /**
     * ImportarController constructor.
     */
    public function __construct()
    {
        $this->produtos = Produto::getAll();
        $this->municipios = Municipio::getAll();
    }

    /**
     * Find the product with a given código.
     *
     * @param string $codigoIbge
     * @return mixed
     */
    private function findProdutoByCodigoIbge($codigoIbge)
    {
        return array_first($this->produtos, function ($produto) use ($codigoIbge) {
            return array_get($produto, 'id') == $codigoIbge;
        });
    }

    /**
     * Find the produto that the nome is the most similar with the given nome.
     * Nomes with less than 85% similarity are discarded.
     *
     * @param $nome
     * @return mixed
     */
    private function findSimilarProdutoByNome($nome)
    {
        $produtos = array_map(function ($produto) use ($nome) {
            similar_text(array_get($produto, 'id'), $nome, $percent);

            $produto['similarity'] = $percent;

            return $produto;
        }, $this->produtos);

        $produtos = array_sort($produtos, function ($produto) {
            return $produto['similarity'];
        });

        $mostSimilarProduto = array_get(array_reverse($produtos), '0');

        return $mostSimilarProduto['similarity'] > 85 ? $mostSimilarProduto : null;
    }

    /**
     * Find the município with a given código.
     *
     * @param string $codigoIbge
     * @return mixed
     */
    private function findMunicipioByCodigoIbge($codigoIbge)
    {
        return array_first($this->municipios, function ($municipio) use ($codigoIbge) {
            return array_get($municipio, 'id') == $codigoIbge;
        });
    }

    /**
     * Set the CSV header offset.
     *
     * @return ImportarController Returns a reference to `$this` to allow chaining.
     */
    private function setCsvHeaderOffset($offset)
    {
        $this->_csvHeaderOffset = $offset;

        return $this;
    }

    /**
     * Set the CSV column delimiter.
     *
     * @return ImportarController Returns a reference to `$this` to allow chaining.
     */
    private function setCsvDelimiter($delimiter)
    {
        $this->_csvDelimiter = $delimiter;

        return $this;
    }

    /**
     * Increment the number of successful records.
     *
     * @return ImportarController Returns a reference to `$this` to allow chaining.
     */
    private function incrementSuccessfulRecordCount()
    {
        $this->_successfulRecordCount++;

        return $this;
    }

    /**
     * Return the number of successful records.
     *
     * @return int
     */
    private function getSuccessfulRecordCount()
    {
        return $this->_successfulRecordCount;
    }

    /**
     * Increment the number of failed records.
     */
    private function incrementFailedRecordCount()
    {
        $this->_failedRecordCount++;
    }

    /**
     * Return the number of failed records.
     *
     * @return int
     */
    private function getFailedRecordCount()
    {
        return $this->_failedRecordCount;
    }

    /**
     * Add an pending record.
     *
     * @param array $record Record to be added.
     * @return ImportarController Returns a reference to `$this` to allow chaining.
     */
    private function addPendingRecord($record)
    {
        $found = false;

        foreach ($this->_pendingRecords as &$pendingRecord) {
            if ($record['produto'] == $pendingRecord['produto']
                && $record['municipio'] == $pendingRecord['municipio']) {
                $found = true;
                $pendingRecord['quantidade'] += $record['quantidade'];
                break;
            }
        }

        if (!$found) {
            $this->_pendingRecords[] = $record;
        }

        return $this;
    }

    /**
     * Return the pending records.
     *
     * @return array
     */
    private function getPendingRecords()
    {
        return $this->_pendingRecords;
    }

    /**
     * @param Request $request
     * @param callable $callback
     * @return Response
     */
    private function importData(Request $request, $callback)
    {
        $file = $request->file('csv');

        if (!$file || !$file->isValid()) {
            return response([
                'error' => true,
                'message' => 'Arquivo inválido.',
            ], 400);
        }

        $encodedContent = Encoding::fixUTF8(File::get($file->path()));
        File::put($file->path(), $encodedContent);

        $csv = Reader::createFromPath($file->path());
        $csv->setHeaderOffset($this->_csvHeaderOffset);
        $csv->setDelimiter($this->_csvDelimiter);

        DB::beginTransaction();

        foreach ($csv->getRecords() as $record) {
            $result = $callback($record);

            if ($result === false) {
                $this->incrementFailedRecordCount();
            } elseif (is_array($result)) {
                $this->incrementSuccessfulRecordCount();
                $this->addPendingRecord($result);
            }
        }

        foreach ($this->getPendingRecords() as $pendingRecord) {
            Producao::create($pendingRecord);
        }

        if ($this->hasErrors() && !$request->get('force', false)) {
            DB::rollback();

            return response([
                'error' => true,
                'message' => 'Ocorreram erros ao importar os dados.',
                'data' => [
                    'errors' => $this->getErrors(),
                    'successfulRecords' => $this->getSuccessfulRecordCount(),
                    'failedRecords' => $this->getFailedRecordCount(),
                ],
            ], 400);
        }

        DB::commit();

        return response([
            'error' => false,
            'message' => 'Dados importados com sucesso!',
            'data' => [
                'successfulRecords' => $this->getSuccessfulRecordCount(),
                'failedRecords' => $this->getFailedRecordCount(),
            ],
        ]);
    }

    /**
     * Data import handler for PEVS (Produção da Extração Vegetal e da Silvicultura).
     *
     * @param Request $request
     * @return Response
     */
    public function pevs(Request $request)
    {
        $this->setCsvHeaderOffset(4);

        return $this->importData($request, function ($record) {
            $codigoMunicipio = array_get($record, 'COD_MUNICIPIO', 0);
            $nomeProduto = array_get($record, 'NOME_PRODUTO', '');
            $ano = array_get($record, 'ANO', 0);
            $quantidade = array_get($record, 'VALOR_VAR1', 0) ?: 0;

            if (!$codigoMunicipio || !$ano || !$nomeProduto) {
                return null;
            }

            $municipio = $this->findMunicipioByCodigoIbge($codigoMunicipio);

            if (!$municipio) {
                $this->addError(sprintf(
                    'Não foi possível encontrar um município com o código do IBGE %s.',
                    $codigoMunicipio
                ));

                return false;
            }

            $produto = $this->findProdutoByCodigoIbge($nomeProduto);

            if (!$produto) {
                $produto = $this->findSimilarProdutoByNome($nomeProduto);

                if (!$produto) {
                    $this->addError(sprintf(
                        'Não foi possível encontrar um produto com o nome %s.',
                        $nomeProduto
                    ));

                    return false;
                }

                $this->addError(sprintf(
                    'O produto sendo importado com nome "%s" será assimilado para o produto cadastrado com nome "%s".',
                    $nomeProduto,
                    $produto['nome']
                ));
            }

            $data = [
                'municipio' => $codigoMunicipio,
                'produto' => $produto['id'],
                'ano' => $ano,
            ];

            $producao = Producao::where($data)->first();

            if ($producao) {
                $this->addError(sprintf(
                    'Dado de produção já existente: Código do Município - %s, Nome do Produto - %s, Ano - %s',
                    $codigoMunicipio, $nomeProduto, $ano
                ));

                return false;
            }

            $data['tipo'] = 'PEVS';
            $data['quantidade'] = $quantidade;

            return $data;
        });
    }

    /**
     * Data import handler for PPM (Produção da Pecuária Municipal).
     *
     * @param Request $request
     * @return Response
     */
    public function ppm(Request $request)
    {
        $this->setCsvHeaderOffset(5)->setCsvDelimiter(';');

        return $this->importData($request, function ($record) {
            $codigoMunicipio = array_get($record, 'COD_MUNICIPIO', 0);
            $nomeProduto = array_get($record, 'NOME_PRODUTO', '');
            $ano = array_get($record, 'ANO', 0);
            $quantidade = array_get($record, 'QTD_ANO_ATUAL', 0) ?: 0;

            if (!$codigoMunicipio || !$ano || !$nomeProduto) {
                return null;
            }

            $municipio = $this->findMunicipioByCodigoIbge($codigoMunicipio);

            if (!$municipio) {
                $this->addError(sprintf(
                    'Não foi possível encontrar um município com o código do IBGE %s.',
                    $codigoMunicipio
                ));

                return false;
            }

            $produto = $this->findProdutoByCodigoIbge($nomeProduto);

            if (!$produto) {
                $produto = $this->findSimilarProdutoByNome($nomeProduto);

                if (!$produto) {
                    $this->addError(sprintf(
                        'Não foi possível encontrar um produto com o nome %s.',
                        $nomeProduto
                    ));

                    return false;
                }

                $this->addError(sprintf(
                    'O produto sendo importado com nome "%s" será assimilado para o produto cadastrado com nome "%s".',
                    $nomeProduto,
                    $produto['nome']
                ));
            }

            $data = [
                'municipio' => $codigoMunicipio,
                'produto' => $produto['id'],
                'ano' => $ano,
            ];

            $producao = Producao::where($data)->first();

            if ($producao) {
                $this->addError(sprintf(
                    'Dado de produção já existente: Código do Município - %s, Nome do Produto - %s, Ano  - %s.',
                    $codigoMunicipio, $nomeProduto, $ano
                ));

                return false;
            }

            $data['tipo'] = 'PPM';
            $data['quantidade'] = $quantidade;

            return $data;
        });
    }

    /**
     * Data import handler for LSPA (Levantamento Sistemático da Produção Agrícola).
     *
     * @param Request $request
     * @return Response
     */
    public function lspa(Request $request)
    {
        $this->setCsvDelimiter(';');

        return $this->importData($request, function ($record) {
            $codigoMunicipio = array_get($record, 'COD_MUNICIPIO', 0);
            $codigoProduto = array_get($record, 'COD_PRODUTO', 0);
            $ano = array_get($record, 'ANO', 0);
            $mes = array_get($record, 'MES', 0);
            $quantidade = array_get($record, 'PRODUCAO', 0);

            if (!$codigoMunicipio || !$codigoProduto || !$ano || !$mes) {
                return null;
            }

            // COD_MUNICIPIO for the LSPA tables are missing the leading digits
            $codigoMunicipio = '320' . str_pad($codigoMunicipio, 4, '0', STR_PAD_LEFT);

            $municipio = $this->findMunicipioByCodigoIbge($codigoMunicipio);

            if (!$municipio) {
                $this->addError(sprintf(
                    'Não foi possível encontrar um município com o código do IBGE %s.',
                    $codigoMunicipio
                ));

                return false;
            }

            $codigoProduto = preg_replace('/[^0-9]+/', '', $codigoProduto);

            $produto = $this->findProdutoByCodigoIbge($codigoProduto);

            if (!$produto) {
                $this->addError(sprintf(
                    'Não foi possível encontrar o produto com o código do IBGE %s.',
                    $codigoProduto
                ));

                return false;
            }

            $data = [
                'municipio' => $codigoMunicipio,
                'produto' => $codigoProduto,
                'ano' => $ano,
                'mes' => $mes,
            ];

            $producao = Producao::where($data)->first();

            if ($producao) {
                $this->addError(sprintf(
                    'Dado de produção já existente: Código do Município - %s, Código do Produto - %s, Ano - %s, Mês - %s.',
                    $codigoMunicipio, $codigoProduto, $ano, $mes
                ));

                return false;
            }

            $data['tipo'] = 'LSPA';
            $data['quantidade'] = $quantidade;

            return $data;
        });
    }
}
