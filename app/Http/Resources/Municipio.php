<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Municipio extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['cd_codigo_ibge'],
            'nome' => $this['nm_municipio'],
        ];
    }
}
