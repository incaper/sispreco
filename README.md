# Sispreco
O projeto visa desenvolver um Sistema de Informação para o Levantamento de preços recebidos pelos pr
dutores rurais – SISPREÇO – para Sistematizar dados atuais e históricos dos levantamentos anteriores
 O projeto está sendo desenvolvido em parceria com professores e alunos do departamento de informáti
a da Ufes campus Goiabeiras.
Atualmente estes dados históricos não estão sistematizados e não estão facilmente acessíveis e o lev
ntamento e organização dos dados e feita manualmente utilizando planilhas. Assim, é necessário siste
atizar todo o levantamento de preços e disponibilizar dados atuais e séries históricas de forma mais
ágil.

Além de servir como fonte de consulta para os produtores rurais e comerciantes, tais levantamentos s
o instrumentos balizadores de mercado, cujos preços levantados são utilizados como referência por di
ersos programas governamentais no estado, como o Programa de Aquisição da Agricultura Familiar – PAA
e o Programa de Garantia de Preço Mínimo - PGPM, executados pela CONAB. O levantamento também é util
zado pelo Instituto Brasileiro de Geografia e Estatística - IBGE e Instituto Jones dos Santos Neves
 IJSN para cálculo do PIB trimestral e anual do estado, bem como para o cálculo do PIB anual dos mun
cípios.

Segue abaixo um link de um documento que trata do levantamento de preços

https://biblioteca.incaper.es.gov.br/digital/bitstream/item/2303/1/BRT-Publicacao-Levantamento-de-Pr
ecos.pdf

## Funcionalidades

### Frontend
- 🚫 Realizar login
- 🚫 Importar dados do IBGE
- 🚫 Realizar levantamento de preço
- 🚫 Gerar relatórios
- 🚫 Definir quais produtos serão utilizados no levantamento em um município
- 🚫 Definir quais usuários podem realizar o levantamento em um município
- 🚫 Definir preço mínimo e máximo para um produto

### Backend
- 🚫 Validar login
- ✅️ Importar dados do IBGE - Tabela PEVS
- ✅ Importar dados do IBGE - Tabela PPM
- ✅ Importar dados do IBGE - Tabbela LSPA
- ✅ Realizar levantamento de preço
- ✅ Definir quais produtos serão utilizados no levantamento de preço em um município.
- ✅ Definir quais usuários podem realizar o levantamento em um município
- ✅ Gerar relatório de preços semanal por produto e por município
- 🚫 Gerar relatório de preços semanal por produto
- 🚫 Gerar relatório de séries históricas
- 🚫 Definir preço mínimo e máximo para um produto
- 🚫 Validar preço do levantamento baseado nos preços mínmos e máximos


## Rotas da API
### Incaper
- `GET /municipios`

  Retorna a lista de muncípios.

- `GET /municipio/{id}`

  Retorna os dados de um município.

- `GET /beneficiarios`

  Retorna a lista de beneficiários.

- `GET /beneficiarios/municipio/{id}`

  Retorna a lista de beneficiários de um município.

- `GET /beneficiario/{id}`

  Retorna os dados de um beneficiário.

- `GET /produtos`

  Retorna a lista de produtos.

- `GET /produto/{id}`

  Retorna os dados de um produto.


### Sispreço

- `GET /produtos`

  Retorna a lista de produtos.

- `GET /produtores/municipio/{id}`

  Retorna a lista de produtores de um município.

- `POST /levantamento`

  Realiza um levantamento de preço.

  **Parâmetros:**
  - `produto` - código do produto
  - `produtor` - código do produtor
  - `municipio` - código do município
  - `preco` - preço

- `GET /levantamento/produtos`

  Retorna a lista de produtos para se realizar o levantamento em um município.

  **Parâmetros:**
  - `municipio` - código do município
  - `ano` - integer - ano do levantamento

- `POST /levantamentos/produtos`

  Atualiza a lista de produtos para se realizar o levantamento em um município.

  **Parâmetros (JSON body):**
  - `municipio` - código do município
  - `ano` - ano do levantamento
  - `produtos` - lista de códigos de produtos

- `GET /levantamento/agentes`

  Retorna a lista de agentes que podem realizar o levantamento em um município.

  **Parâmetros:**
  - `municipio` - código do município

- `POST /levantamentos/agentes`

  Atualiza a lista de agentes que podem realizar o levantamento em um município.

  **Parâmetros (JSON body):**
  - `municipio` - código do município
  - `agentes` - lista de identificadores de agentes

- `POST /importar/pevs`

  Importa os dados do IBGE - Tabela PEVS.

  **Parâmetros:**
  - `csv` - arquivo csv para ser importado
  - `force` - ignora os erros e força a importação se presente (opcional)

- `POST /importar/ppm`

  **Parâmetros:**
  - `csv` - arquivo csv para ser importado
  - `force` - ignora os erros e força a importação se presente (opcional)

  Importa os dados do IBGE - Tabela PPM.

- `POST /importar/lspa`

  Importa os dados do IBGE - Tabela LSPA.

  **Parâmetros:**
  - `csv` - arquivo csv para ser importado
  - `force` - ignora os erros e força a importação se presente (opcional)

- `POST /relatorio/gerar`

