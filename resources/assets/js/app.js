window.$ = window.jQuery = require('jquery');
window.Vue = require('vue');
// window.Oidc = require('oidc-client');

import 'bootstrap';
import Vuex from 'vuex'
import VueResource from 'vue-resource'

// Oidc.Log.logger = console;
// Oidc.Log.level = Oidc.Log.INFO;
//
// window.oidcclient = new Oidc.OidcClient({
//   authority: 'https://identidade.incaper.es.gov.br:8443/auth/realms/sispreco/',
//   client_id: 'sispreco',
//   redirect_uri: 'http://sispreco.docker/auth',
//   post_logout_redirect_uri: 'http://sispreco.docker',
//   response_type: 'id_token token',
//   scope: 'openid email roles',
//
//   filterProtocolClaims: true,
//   loadUserInfo: true
// });

Vue.use(Vuex)
Vue.use(VueResource)
Vue.http.options.root = '/sispreco/api'

const store = new Vuex.Store({
    state: {
        currentComponent: 'inicio',
        user: {
            name: 'Rômulo Vitoi',
        },
        municipios: null,
        produtos: null,
        produtores: {},
        produtosLevantamento: {},
        anosLevantamento: null,
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload
        },
        removeUser: state => state.user = null,
        setComponent(state, payload) {
            state.currentComponent = payload
        },
        setMunicipios(state, payload) {
            state.municipios = payload
        },
        setProdutos(state, payload) {
            state.produtos = payload
        },
        addProdutores(state, payload) {
            state.produtores = {...state.produtores}
            state.produtores[payload.municipio.id] = payload.produtores
        },
        addProdutosLevantamento(state, payload) {
            state.produtosLevantamento = {...state.produtosLevantamento}
            state.produtosLevantamento[payload.municipio.id] = payload.produtos
        },
        resetProdutosLevantamento(state) {
            state.produtosLevantamento = {}
        },
        setAnosLevantamento(state, payload) {
            state.anosLevantamento = payload
        },
    },
    actions: {
        login(context) {
            context.commit('setUser', {
                name: 'Rômulo Vitoi',
            })
        },
        logout(context) {
            context.commit('removeUser')
            context.commit('setComponent', 'inicio')
        },
        setComponent(context, payload) {
            context.commit('setComponent', payload)
        },
        getMunicipios(context) {
            if (context.state.municipios) {
                return
            }

            Vue.http.get('municipios').then(response => {
                return response.json()
            }).then(data => {
                context.commit('setMunicipios', data.data)
            }).catch(error => {
                console.error('Falha ao listar municipios: ' + error)
            });
        },
        getProdutos(context) {
            if (context.state.produtos) {
                return;
            }

            Vue.http.get('produtos')
                .then(response => response.json())
                .then(data => {
                    context.commit('setProdutos', data.data)
                })
                .catch(error => {
                    console.error('Falha ao listar produtos: ' + error)
                });
        },
        atualizaProduto(context, payload) {
            const produtos = context.state.produtos.map(produto => {
                if(produto.id === payload.id) {
                    produto.preco_minimo = payload.preco_minimo
                    produto.preco_maximo = payload.preco_maximo
                }

                return produto;
            })

            context.commit('setProdutos', produtos)
            context.commit('resetProdutosLevantamento')
        },
        getProdutores(context, payload) {
            if (context.state.produtores[payload.id]) {
                return
            }

            Vue.http.get('produtores/municipio/' + payload.id)
                .then(response => response.json())
                .then(data => {
                    context.commit('addProdutores', {municipio: payload, produtores: data.data})
                })
                .catch(error => {
                    console.error('Falha ao listar beneficiarios: ' + error)
                })
        },
        getProdutosLevantamento(context, payload) {
            // if (context.state.produtosLevantamento[payload.id]) {
            //     return
            // }

            Vue.http.get('levantamento/produtos', {
                params: {
                    municipio: payload.id,
                },
            })
                .then(response => response.json())
                .then(data => {
                    context.commit('addProdutosLevantamento', {municipio: payload, produtos: data.data})
                })
                .catch(error => {
                    console.error('Falha ao listar produtos para levantamento: ' + error)
                })
        },
        getAnosLevantamento(context) {
            Vue.http.get('levantamento/anos')
                .then(response => response.json())
                .then(data => {
                    context.commit('setAnosLevantamento', data.data)
                })
                .catch(error => {
                    console.error('Falha ao listar anos dos levantamentos: ' + error)
                })
        },
    }
})

var app = new Vue({
    el: '#app',
    store,
    computed: Vuex.mapState([
        'currentComponent'
    ]),
    mounted() {
        this.$store.dispatch('getMunicipios')
        this.$store.dispatch('getProdutos')
        this.$store.dispatch('getAnosLevantamento')
    },
    components: {
        'page-header': require('./components/PageHeader.vue'),
        'nav-bar': require('./components/NavBar.vue'),
        'inicio': require('./components/Inicio.vue'),
        'levantamento': require('./components/Levantamento.vue'),
        'definicao-produtos': require('./components/DefinicaoProdutos.vue'),
        'importar': require('./components/Importar.vue'),
        'intervalo': require('./components/Intervalo.vue'),
        'consulta-levantamento': require('./components/ConsultaLevantamento.vue'),
        'consulta-detalhada': require('./components/ConsultaDetalhada.vue'),
        'consulta-series': require('./components/ConsultaSeries.vue'),
    },
});
